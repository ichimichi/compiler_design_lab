#include <stdio.h>
#include <malloc.h>
#include <string.h>

typedef enum
{
    STATE1,
    STATE2,
    STATE3,
    STATE4,
    STATE5
} DFAStateOperator;

const DFAStateOperator transitionTableOperator[5][3] = {
    {STATE2, STATE1, STATE1},
    {STATE5, STATE3, STATE4},
    {STATE1, STATE1, STATE1},
    {STATE1, STATE1, STATE1},
    {STATE1, STATE1, STATE1}};

void recognizeDFAOperator(char input[])
{
    DFAStateOperator currentState = STATE1;

    for (int i = 0; i <= strlen(input); i++)
    {
        char ch = input[i];

        switch (input[i])
        {
        case '+':
            currentState = transitionTableOperator[currentState][0];
            break;
        case '=':
            currentState = transitionTableOperator[currentState][1];
            break;
        default:
            currentState = transitionTableOperator[currentState][2];
            break;
        }

        switch (currentState)
        {
        case STATE3:
            printf("<ADD_ASN>");
            currentState = STATE1;
            break;
        case STATE4:
            printf("<ADD>");
            currentState = STATE1;
            i--;
            break;
        case STATE5:
            printf("<INCR>");
            currentState = STATE1;
            break;
        default:
            break;
        }
    }
}

int main()
{
    FILE *file;
    char *buffer;
    int numBytes;

    {
        file = fopen("1/file-02", "rb");

        if (file == NULL)
        {
            printf("Error opening file");
            return 1;
        }

        fseek(file, 0L, SEEK_END);
        numBytes = ftell(file);
        fseek(file, 0L, SEEK_SET);

        buffer = (char *)malloc( (numBytes +1 )* sizeof(char));

        if (buffer == NULL)
        {
            printf("Error memory");
            return 1;
        }

        fread(buffer, sizeof(char), numBytes, file);

        *(buffer + numBytes) = '\0';
        fclose(file);
    }

    recognizeDFAOperator(buffer);
    free(buffer);
    return 0;
}