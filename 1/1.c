#include <stdio.h>
#include <malloc.h>
#include <string.h>

typedef enum
{
    STATE1,
    STATE2,
    STATE3,
    STATE4,
    STATE5,
    STATE6,
    STATE7
} DFAState;

void recognizeDFA(char input[])
{
    DFAState currentState = STATE1;

    for (int i = 0; i <= strlen(input) + 1; i++)
    {
        char ch = input[i];
        switch (currentState)
        {
        case STATE1:
        {
            switch (input[i])
            {
            case '>':
                currentState = STATE2;
                continue;
            case '=':
                currentState = STATE1;
                continue;
            default:
                currentState = STATE1;
                continue;
            }
        }
        case STATE2:
        {
            switch (input[i])
            {
            case '>':
                currentState = STATE3;
                continue;
            case '=':
                currentState = STATE6;
                continue;
            default:
                currentState = STATE7;
                continue;
            }
        }
        case STATE3:
        {
            switch (input[i])
            {
            // case '>':
            //     currentState = STATE1;
            //     continue;
            case '=':
                currentState = STATE4;
                continue;
            default:
                currentState = STATE5;
                continue;
            }
        }
        case STATE4:
        {
            printf("<RIGHT_SHIFT_ASN>");
            currentState = STATE1;
            i--;
            continue;
        }
        case STATE5:
        {
            printf("<RIGHT_SHIFT>");
            i -= 2;
            currentState = STATE1;
            continue;
        }
        case STATE6:
        {
            printf("<GREATER_THAN_EQUAL>");
            currentState = STATE1;
            i--;
            continue;
        }
        case STATE7:
        {
            printf("<GREATER_THAN>");
            i -= 2;
            currentState = STATE1;
            continue;
        }
        }
    }
}

int main()
{
    FILE *file;
    char *buffer;
    int numBytes;

    file = fopen("1/file-01", "r");

    if (file == NULL)
    {
        printf("Error opening file");
        return 1;
    }

    fseek(file, 0L, SEEK_END);
    numBytes = ftell(file);
    fseek(file, 0L, SEEK_SET);

    buffer = (char *)malloc((numBytes + 1) * sizeof(char));

    if (buffer == NULL)
    {
        printf("Error memory");
        return 1;
    }

    fread(buffer, sizeof(char), numBytes, file);
    *(buffer + numBytes) = '\0';
    fclose(file);

    printf("%s\n", buffer);

    // printf("Enter: ");
    // scanf("%[^\n]", buffer);

    recognizeDFA(buffer);
    free(buffer);
    return 0;
}