#include <stdio.h>
#include <malloc.h>
#include <string.h>

typedef enum
{
    STATE1,
    STATE2,
    STATE3,
    STATE4,
    STATE5,
    STATE6,
    STATE7
} DFAState;

const DFAState transitionTable[7][3] = {
    {STATE2, STATE1, STATE1},
    {STATE3, STATE6, STATE7},
    {STATE5, STATE4, STATE5},
    {STATE1, STATE1, STATE1},
    {STATE1, STATE1, STATE1},
    {STATE1, STATE1, STATE1},
    {STATE1, STATE1, STATE1}};

void recognizeDFA(char input[])
{
    DFAState currentState = STATE1;

    for (int i = 0; i <= strlen(input); i++)
    {
        char ch = input[i];

        switch (input[i])
        {
        case '>':
            currentState = transitionTable[currentState][0];
            break;
        case '=':
            currentState = transitionTable[currentState][1];
            break;
        default:
            currentState = transitionTable[currentState][2];
            break;
        }

        switch (currentState)
        {
        case STATE4:
            printf("<RIGHT_SHIFT_ASN>");
            currentState = STATE1;
            break;
        case STATE5:
            printf("<RIGHT_SHIFT>");
            i--;
            currentState = STATE1;
            break;
        case STATE6:
            printf("<GREATER_THAN_EQUAL>");
            currentState = STATE1;
            continue;
        case STATE7:
            printf("<GREATER_THAN>");
            i--;
            currentState = STATE1;
            break;
        default:
            break;
        }
    }
}

int main()
{
    FILE *file;
    char *buffer;
    int numBytes;

    {
        file = fopen("1/file-01", "rb");

        if (file == NULL)
        {
            printf("Error opening file");
            return 1;
        }

        fseek(file, 0L, SEEK_END);
        numBytes = ftell(file);
        fseek(file, 0L, SEEK_SET);

        buffer = (char *)calloc((numBytes + 1), sizeof(char));

        if (buffer == NULL)
        {
            printf("Error memory");
            return 1;
        }

        int result = fread(buffer, sizeof(char), numBytes, file);
        *(buffer + numBytes) = '\0';
        fclose(file);
    }

    recognizeDFA(buffer);
    free(buffer);
    return 0;
}