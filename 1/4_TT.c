#include <stdio.h>
#include <malloc.h>
#include <string.h>

typedef enum
{
    STATE1,
    STATE2,
    STATE3,
    STATE4,
    STATE5,
    STATE6,
    STATE7,
    STATE8,
    STATE9
} DFAState;

const DFAState transitionTable[9][5] = {
    //  +      =      d     .    others
    {STATE2, STATE1, STATE6, STATE1, STATE1},
    {STATE5, STATE3, STATE4, STATE4, STATE4},
    {STATE1, STATE1, STATE1, STATE1, STATE1},
    {STATE1, STATE1, STATE1, STATE1, STATE1},
    {STATE1, STATE1, STATE1, STATE1, STATE1},
    {STATE1, STATE1, STATE6, STATE7, STATE1},
    {STATE1, STATE1, STATE8, STATE1, STATE1},
    {STATE9, STATE9, STATE8, STATE9, STATE9},
    {STATE1, STATE1, STATE1, STATE1, STATE1}};

void recognizeDFA(char input[])
{
    DFAState currentState = STATE1;

    for (int i = 0; i <= strlen(input); i++)
    {
        char ch = input[i];

        switch (input[i])
        {
        case '+':
            currentState = transitionTable[currentState][0];
            break;
        case '=':
            currentState = transitionTable[currentState][1];
            break;
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
            currentState = transitionTable[currentState][2];
            break;
        case '.':
            currentState = transitionTable[currentState][3];
            break;
        default:
            currentState = transitionTable[currentState][4];
            break;
        }

        switch (currentState)
        {
        case STATE3:
            printf("<ADD_ASN>");
            currentState = STATE1;
            break;
        case STATE4:
            printf("<ADD>");
            currentState = STATE1;
            i--;
            break;
        case STATE5:
            printf("<INCR>");
            currentState = STATE1;
            break;
        case STATE9:
            printf("<FRACT_NUM>");
            i--;
            currentState = STATE1;
            break;
        default:
            break;
        }
    }
}

int main()
{
    FILE *file;
    char *buffer;
    int numBytes;

    {
        file = fopen("1/file-04a", "r");

        if (file == NULL)
        {
            printf("Error opening file");
            return 1;
        }

        fseek(file, 0L, SEEK_END);
        numBytes = ftell(file);
        fseek(file, 0L, SEEK_SET);

        buffer = (char *)malloc((numBytes + 1) * sizeof(char));

        if (buffer == NULL)
        {
            printf("Error memory");
            return 1;
        }

        fread(buffer, sizeof(char), numBytes, file);
        *(buffer + numBytes) = '\0';
        fclose(file);
    }

    recognizeDFA(buffer);
    free(buffer);
    return 0;
}