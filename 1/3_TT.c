#include <stdio.h>
#include <malloc.h>
#include <string.h>

typedef enum
{
    STATE1,
    STATE2,
    STATE3,
    STATE4,
    DEADSTATE5
} DFAStateFractional;

const DFAStateFractional transitionTableFractional[5][3] = {
    {STATE2, DEADSTATE5, DEADSTATE5},
    {STATE2, STATE3, DEADSTATE5},
    {STATE4, DEADSTATE5, DEADSTATE5},
    {STATE4, DEADSTATE5, DEADSTATE5},
    {DEADSTATE5, DEADSTATE5, DEADSTATE5}};

void recognizeDFAFractional(char input[])
{
    DFAStateFractional currentState = STATE1;

    for (int i = 0; i < strlen(input); i++)
    {
        char ch = input[i];

        switch (input[i])
        {
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
            currentState = transitionTableFractional[currentState][0];
            continue;
        case '.':
            currentState = transitionTableFractional[currentState][1];
            continue;
        default:
            currentState = transitionTableFractional[currentState][2];
            continue;
        }
    }

    if (currentState == STATE4)
    {
        printf("<FRACT_NUM>");
    }
}

int main()
{
    FILE *file;
    char *buffer;
    int numBytes;

    file = fopen("1/file-03", "r");

    if (file == NULL)
    {
        printf("Error opening file");
        return 1;
    }

    fseek(file, 0L, SEEK_END);
    numBytes = ftell(file);
    fseek(file, 0L, SEEK_SET);

    buffer = (char *)malloc( (numBytes +1 )* sizeof(char));

    if (buffer == NULL)
    {
        printf("Error memory");
        return 1;
    }

    fread(buffer, sizeof(char), numBytes, file);
    *(buffer + numBytes) = '\0';
    fclose(file);

    recognizeDFAFractional(buffer);
    free(buffer);
    return 0;
}