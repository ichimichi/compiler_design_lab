#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include "interface.h"

int isAlphabet(char x)
{
    if (x >= '0' && x <= '9' || x == '.' || x == '+' || x == '=')
    {
        return 1;
    }
    return 0;
}

int main()
{
    FILE *file;
    char *buffer, ch;
    int numBytes, i, k;
    char lexeme[50];

    file = fopen("1/file-04", "r");

    if (file == NULL)
    {
        printf("Error opening file");
        return 1;
    }

    fseek(file, 0L, SEEK_END);
    numBytes = ftell(file);
    fseek(file, 0L, SEEK_SET);

    buffer = (char *)malloc((numBytes + 1) * sizeof(char));

    if (buffer == NULL)
    {
        printf("Error memory");
        return 1;
    }

    fread(buffer, sizeof(char), numBytes, file);
    fclose(file);

    for (i = 0, k = 0; i <= numBytes; i++)
    {
        ch = buffer[i];
        if (isAlphabet(buffer[i]))
        {
            lexeme[k++] = buffer[i];
        }
        else
        {
            lexeme[k] = '\0';
            k = 0;
            recognizeDFAFractional(lexeme);
            recognizeDFAOperator(lexeme);
        }
    }

    free(buffer);
    return 0;
}