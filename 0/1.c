#include <stdio.h>
#include <ctype.h>

int main()
{
    FILE *file;

    int characters, words, lines;
    char ch;

    characters = words = lines = 0;

    file = fopen("0/file-01", "r");
    if (file == NULL)
    {
        printf("Error opening file");
        return 1;
    }

    while (1)
    {
        ch = fgetc(file);

        printf("%c", ch);
        
        if (ch == EOF)
        {
            words++;
            lines++;
            break;
        }
        else
        {
            if (tolower(ch) >= 'a' && tolower(ch) <= 'z')
            {
                characters++;
            }
            else if (ch == ' ')
            {
                words++;
            }
            else if (ch == '\n')
            {
                lines++;
                words++;
            }
        }
        
    }
    printf("\ncharacters : %d", characters);
    printf("\nwords : %d", words);
    printf("\nlines : %d", lines);

    fclose(file);
}
