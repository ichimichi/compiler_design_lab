#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "token.h"
#include "lex.yy.c"

int i;
int token;

int nextToken()
{
    return yylex();
}

int check(int ch);

void E();
void E1();
void T();
void T1();
void F();

int main()
{
    token = nextToken();

    E();

    if (token == '$')
    {
        printf("valid\n");
    }
    else
    {
        printf("invalid\n");
    }

    return 0;
}

void E()
{
    switch (token)
    {
    case '(':
    {
        T();
        E1();
        break;
    }
    case id:
    {
        T();
        E1();
        break;
    }
    default:
    {
        printf("invalid\n");
        exit(1);
    }
    }
}

void E1()
{
    switch (token)
    {
    case '+':
    {
        check('+');
        T();
        E1();
        break;
    }
    case ')':
    {
        return;
    }
    case '$':
    {
        return;
    }
    default:
    {
        printf("invalid\n");
        exit(1);
    }
    }
    return;
}

void T()
{

    switch (token)
    {
    case '(':
    {
        F();
        T1();
        break;
    }
    case id:
    {
        F();
        T1();
        break;
    }
    default:
    {
        printf("invalid\n");
        exit(1);
    }
    }
}

void T1()
{
    switch (token)
    {
    case '*':
    {
        check('*');
        F();
        T1();
        break;
    }
    case '+':
    {
        return;
    }
    case ')':
    {
        return;
    }
    case '$':
    {
        return;
    }
    default:
    {
        printf("invalid\n");
        exit(1);
    }
    }
}

void F()
{
    switch (token)
    {
    case '(':
    {
        check('(');
        E();
        switch (token)
        {
        case ')':
        {
            check(')');
            break;
        }
        default:
        {
            printf("invalid\n");
            exit(1);
        }
        }
        break;
    }
    case id:
    {
        check(id);
        break;
    }
    default:
    {
        printf("invalid\n");
        exit(1);
    }
    }
}

int check(int ch)
{
    if (token == ch)
    {
        token = nextToken();
    }
    else
    {
        printf("invalid\n");
        exit(1);
    }
}