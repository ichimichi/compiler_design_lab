%{
	#include <stdio.h>
    #include "lex.yy.c"
	void yyerror(char *);
%}
%token NUM
%left '+' '-'
%left '*' '/'
%%
List: 	List  E  '\n'          {printf("=%d\n",$2);}
	|		// List ->  epsilon
	;
E:        NUM
	|E '+' E 	{$$=$1+$3;}  
	|E '-' E 	{$$=$1-$3;} 
	|E '*' E 	{$$=$1*$3;} 
	|E '/' E 	{$$=$1/$3;} 
	|'(' E ')' 	{$$=$2;} 
	;

%%
int main()
{ 	yyparse();    }

void yyerror(char *s)
{  }