%{
	#include "lex.yy.c"
	void yyerror();
%}

%token  NUM

%%
e : e '+' t 	{ printf("+ ");}
  | e '-' t 	{ printf("- ");}
  | t
  ;
t : t '*' f 	{ printf("* ");}
  | t '/' f	{ printf("/ ");}
  | f
  ;
f : '(' e ')' 
| NUM { printf("%d ",$1);}
  ;
%%
int main()
{
 yyparse();
}
void yyerror()
{
}