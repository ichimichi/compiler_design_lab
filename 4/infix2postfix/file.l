%option noyywrap
%{
    #include "file.tab.h"
%}
%%
[0-9]+ { yylval=atoi(yytext);return NUM;}  
"+"	{ return '+'; }
"-"	{ return '-'; }
"*"	{ return '*'; }
"/"	{ return '/'; }
"("	{ return '('; }
")"	{ return ')'; }
"$"	{ return 0;     /*end of input characters */  }
[ \t] { }
\n { }
%%